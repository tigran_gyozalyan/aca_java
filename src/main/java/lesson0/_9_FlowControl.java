package lesson0;

import java.util.concurrent.TimeUnit;

public class _9_FlowControl {
   public static void main(String[] args) {


      //region init
      boolean someBooleanExpression = 1 < 2;
      int[] numbers = {10, 20, 30, 40, 50};
      //endregion

      //region if else

      if (someBooleanExpression) {
         // Executes when the Boolean expression is true
      } else if (someBooleanExpression) {
         // Executes when the Boolean expression is false
      }

      String something = someBooleanExpression ? "if true do this" : "else do that";

      //endregion

      //region switch

      switch (5) {
         case 1:
            // do something
            break; // optional

         case 2:
            // do something
            break; // optional

         // You can have any number of case statements.
         default: // Optional
            // do something
      }

      String str = "b";

      switch (str) {
         case "b":
            // do something
            break; // optional
         case "c":
            // do something
            break; // optional

         // You can have any number of case statements.
         default: // Optional
            // do something
      }

      TimeUnit someEnum = TimeUnit.MILLISECONDS;
      switch (someEnum) {
         case DAYS:
            //
      }
      //endregion

      //region looping
      for (int x = 10; x < 20; x = x + 1) {
         System.out.print("value of x : " + x);
         System.out.print("\n");
      }

      for (int x : numbers) {
         System.out.print(x);
      }


      while (someBooleanExpression) {
         // do something
      }

      do {
         // do something
      } while (someBooleanExpression);


      // break
      for (int x : numbers) {
         if (x == 30) {
            break;
         }
         System.out.print(x);
         System.out.print("\n");
      }


      for (int x : numbers) {
         if (x == 30) {
            continue;
         }
         System.out.print(x);
         System.out.print("\n");
      }

   }

}
