package lesson2;

import java.util.Arrays;

public class _9_HashMap {


   static class MyMap<K, V> {

      static final class Entry<K, V> {
         final K k;
         final V v;

         Entry(K k, V v) {
            this.k = k;
            this.v = v;
         }

         @Override
         public String toString() {
            return String.format("Entry{k=%s, v=%s}", k, v);
         }
      }


      private Entry[] arr = new Entry[10];
      private int size = 0;

      public V put(V key, V value) {
         int hash = Math.abs(key.hashCode());
         int index = hash % arr.length;

         V replaced = null;
         if (arr[index] == null) {
            arr[index] = new Entry(key, value);
         } else {
            Entry existing = arr[index];
            if (existing.k.equals(key)) {
               // replace
               replaced = (V) existing.v;
               arr[index] = new Entry(key, value);
            } else {
               // collision (two different keys hit the same bucket)
               throw new RuntimeException("collision resolution not implemented");
            }
         }

         return replaced;
      }

      public V get(K key) {
         int hash = Math.abs(key.hashCode());
         int index = hash % arr.length;

         Entry entry = arr[index];
         if (entry != null) {
            return (V) entry.v;
         } else {
            return null;
         }
      }

      public int size() {
         return 0;
      }

      public V remove(K key) {
         return null;
      }

      public void print() {
         System.out.println(Arrays.toString(arr));
      }
   }

   public static void main(String[] args) {
      MyMap<String, String> map = new MyMap<>();

      map.put("bob marley", "singer");
      map.print();
      System.out.println(map.get("bob marley"));
   }
}
