package homeworks.vachagan_balayan.lesson1.roulette;

import lesson1._roulette.interfaces.IDealer;
import lesson1._roulette.interfaces.IPlayer;
import lesson1._roulette.interfaces.IStrategy;

public class Player implements IPlayer {

   private int cash;
   private final IStrategy strategy;

   public Player(int startCash, IStrategy strategy) {
      this.cash = startCash;
      this.strategy = strategy;
   }

   @Override
   public boolean hasMoney() {
      return cash > 0;
   }

   @Override
   public void playWith(IDealer dealer) {
      IStrategy.IBet bet = strategy.nextBet();
      int amount = bet.getAmount();

      if (cash < amount) {
         amount = cash; // all in
      }

      dealer.takeBet(this, bet.getColor(), amount);
   }

   @Override
   public void debit(int amount) {
      if (cash < amount) {
         throw new RuntimeException("out of cash");
      }

      strategy.registerFailure();
      cash -= amount;
   }

   @Override
   public void credit(int amount) {
      strategy.registerSuccess();
      cash += amount;
   }

   @Override
   public int getCash() {
      return cash;
   }
}
