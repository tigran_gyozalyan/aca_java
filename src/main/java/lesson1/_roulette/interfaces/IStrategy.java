package lesson1._roulette.interfaces;

public interface IStrategy {
   interface IBet {

      int getAmount();
      IRoulette.Color getColor();

   }

   void registerSuccess();

   void registerFailure();

   IBet nextBet();
}
