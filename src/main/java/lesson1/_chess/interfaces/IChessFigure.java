package lesson1._chess.interfaces;

public interface IChessFigure {

   String getName();

   Color getColor();

   boolean canMove(IPosition position1, IPosition position2);
}