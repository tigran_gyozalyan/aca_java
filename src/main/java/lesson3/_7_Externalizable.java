package lesson3;

import java.io.*;
import java.util.Arrays;

import static lesson3.Shared.log;

public class _7_Externalizable {

   static class User implements Externalizable {
      String firstName;
      String lastName;
      int age;

      User() {
      }

      User(String firstName, String lastName, int age) {
         this.firstName = firstName;
         this.lastName = lastName;
         this.age = age;
      }

      @Override
      public String toString() {
         return "User{" +
            "firstName='" + firstName + '\'' +
            ", lastName='" + lastName + '\'' +
            ", age=" + age +
            '}';
      }

      @Override
      public void writeExternal(ObjectOutput out) throws IOException {
         out.writeUTF(firstName);
         out.writeUTF(lastName);
         out.writeInt(age);
      }

      @Override
      public void readExternal(ObjectInput in) throws IOException {
         firstName = in.readUTF();
         lastName = in.readUTF();
         age = in.readInt();
      }
   }


   public static void main(String[] args) throws IOException {
      byte[] bytes = null;
      User user = new User("Jon", "Doe", 42);

      try (
         ByteArrayOutputStream bos = new ByteArrayOutputStream();
         ObjectOutputStream out = new ObjectOutputStream(bos)
      ) {
         log("writing : " + user);
         out.writeInt(0);
         user.writeExternal(out);
         out.flush();
         bytes = bos.toByteArray();
      }

      System.out.println(bytes.length + " bytes : " + Arrays.toString(bytes));

      try (
         ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
         ObjectInputStream in = new ObjectInputStream(bis)
      ) {
         in.readInt();
         User user2 = new User();
         user2.readExternal(in);
         log("reading : " + user);
      }
   }


}
